The changes-package allows the user to manually markup changes of text, such as additions, deletions, or replacements.
Changed text is shown in a different color; deleted text is striked out.
Additionally, text can be highlighted and/or commented.
The package allows free definition of additional authors and their associated color.
It also allows you to change the markup of changes, authors, highlights or comments.

Release 3.1.0 introduces a new script for markup removal. Furthermore, the manual has been updated.
Release 3.1.3 is a bugfix for an option clash for ulem and truncate and introduces documentation of known problems and solutions.

For a list of changes see: https://gitlab.com/ekleinod/changes/blob/master/changelog.md

Please inform me about any errors or improvements as you see fit.
